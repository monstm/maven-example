package io.gitlab.monstm.mavenlibrary;

/**
 * Basic maven class.
 */
public class MavenClass {
	/**
	 * Hello world!
	 *
	 * @return String
	 *
	 */
    public String HelloWorld(){
        return "Hello World";
    }
}
